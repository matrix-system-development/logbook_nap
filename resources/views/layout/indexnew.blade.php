@extends('masternew')
@section('after_style')
@endsection
@section('content')
@section('title','Guest Book Nap Info')
@section('content') 

<script type="text/javascript">window.setTimeout("document.getElementById('successMessage').style.display='none';", 2000); </script>

        {{-- <a class="btn btn-icon icon-left btn-info" href="{{url('exportguest')}}" class="btn btn-success" ><i class="fa fa-file-excel-o" aria-hidden="true"></i> Export</a> --}}

        <a class="navbar-brand text-white">
            <img src="{{ asset('images') }}/matrixlogo.png" alt="" width="120" height="50">
          </a>
        <div class="jumbotron" id="home">
            <div class="container">
                <form action="{{ route('admin') }}" method="GET" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-row">
                      <div class="form-group col-md-4">
                        <label for="searchAll"> Search Company: </label>
                        <input type="text"  name="searchAll" id="searchAll" class="form-control" placeholder="Input your company" value="{{request()->get('searchAll')}}">
                      </div>
                      <div class="form-group col-md-4">
                        <label for="search"> Lokasi :</label>
                        <select name="search" id="search" class="form-control" >
                            <option value="">- Pilih Lokasi -</option>
                            <option @if(request()->get('search')=="1") selected @endif value="1" >MDC JK1 - Pantai Mutiara</option>
                            <option @if(request()->get('search')=="2") selected @endif value="2" >MDC JK2 - Cyber 1</option>
                            <option @if(request()->get('search')=="3") selected @endif value="3" >MDC JK3 - Plaza Kuningan</option>
                            <option @if(request()->get('search')=="4") selected @endif value="4" >MDC BM1 - Nongsa</option>
                            <option @if(request()->get('search')=="5") selected @endif value="5" >MDC SG1 - North Changi</option>
                        </select>
                      </div>
                      <div class="form-group col-md-2">
                        <label for="search1"> Date From :</label>
                        <input type="date"  name="search1" id="search1" class="form-control" placeholder="from" value="{{request()->get('search1')}}">
                      </div>
                      <div class="form-group col-md-2">
                        <label for="search2"> To :</label>
                        <input type="Date" name="search2" id="search2" class="form-control" placeholder="to" value="{{request()->get('search2')}}" >
                      </div>
                    </div>
                    <div style="display: flex; justify-content: center;">
                        <label>
                            Search or Export :
                        </label>
                    </div>
                    <div style="display: flex; justify-content: center;">
                        <button type="submit" class="btn btn-primary mr-1" >
                            {{-- <i class="fa fa-search" aria-hidden="true"></i> --}}
                            {{-- <i class="nav-icon fas fa-search"></i>  --}}
                            <i class="bi bi-search"></i>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                              </svg>
                        </button>
                        <a  class="btn btn-primary mr-1" href="{{url('/admin')}}"> 
                            {{-- <i class="fa fa-history" aria-hidden="true"></i> --}}
                            {{-- <i class="nav-icon fas fa-history"></i> --}}
                            <i class="bi bi-arrow-clockwise"></i>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-clockwise" viewBox="0 0 16 16">
                                <path fill-rule="evenodd" d="M8 3a5 5 0 1 0 4.546 2.914.5.5 0 0 1 .908-.417A6 6 0 1 1 8 2v1z"/>
                                <path d="M8 4.466V.534a.25.25 0 0 1 .41-.192l2.36 1.966c.12.1.12.284 0 .384L8.41 4.658A.25.25 0 0 1 8 4.466z"/>
                              </svg>
                        </a>
                        <button name="guestexport" class="btn btn-success mr-1"> 
                            {{-- <i class="fa fa-file-excel-o" aria-hidden="true"></i> --}}
                            {{-- <i class="nav-icon fas fa-file-excel-o"></i> --}}
                            <i class="bi bi-file-earmark-excel-fill"></i>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-file-earmark-excel-fill" viewBox="0 0 16 16">
                                <path d="M9.293 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.707A1 1 0 0 0 13.707 4L10 .293A1 1 0 0 0 9.293 0zM9.5 3.5v-2l3 3h-2a1 1 0 0 1-1-1zM5.884 6.68 8 9.219l2.116-2.54a.5.5 0 1 1 .768.641L8.651 10l2.233 2.68a.5.5 0 0 1-.768.64L8 10.781l-2.116 2.54a.5.5 0 0 1-.768-.641L7.349 10 5.116 7.32a.5.5 0 1 1 .768-.64z"/>
                              </svg>
                        </button>
                        <button type="submit" name="guestprint"  class="btn btn-success mr-1" formtarget="_blank"> 
                            {{-- <i class="fa fa-print" aria-hidden="true"></i> --}}
                            {{-- <i class="nav-icon fas fa-envelope"></i> --}}
                            {{-- <i class="nav-icon fas fa-print"></i> --}}
                            <i class="bi bi-printer"></i>
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-printer" viewBox="0 0 16 16">
                                <path d="M2.5 8a.5.5 0 1 0 0-1 .5.5 0 0 0 0 1z"/>
                                <path d="M5 1a2 2 0 0 0-2 2v2H2a2 2 0 0 0-2 2v3a2 2 0 0 0 2 2h1v1a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2v-1h1a2 2 0 0 0 2-2V7a2 2 0 0 0-2-2h-1V3a2 2 0 0 0-2-2H5zM4 3a1 1 0 0 1 1-1h6a1 1 0 0 1 1 1v2H4V3zm1 5a2 2 0 0 0-2 2v1H2a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h12a1 1 0 0 1 1 1v3a1 1 0 0 1-1 1h-1v-1a2 2 0 0 0-2-2H5zm7 2v3a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1v-3a1 1 0 0 1 1-1h6a1 1 0 0 1 1 1z"/>
                              </svg>
                        </button>
                    </div>
                </form>
            </div>
        </div>

            @if(session('message'))
                <div class="alert alert-success alert-dismissible show fade" id="successMessage">
                    <div class="alert-body">
                        {{-- <button class="close" data-dismiss="alert">
                            <span>&times;</span>
                        </button> --}}
                        {{session('message')}}
                    </div>
                </div>
                @elseif(session('gagal'))
                <div class="alert alert-warning alert-dismissible show fade" id="successMessage">
                    <div class="alert-body">
                        {{-- <button class="close" data-dismiss="alert">
                            <span>&times;</span>
                        </button> --}}
                        {{session('gagal')}}
                    </div>
                </div>
            @endif
            @if(session('hapus'))
            <div class="alert alert-danger alert-dismissible show fade" id="successMessage">
                <div class="alert-body">
                    {{-- <button class="close" data-dismiss="alert">
                        <span>&times;</span>
                    </button> --}}
                    {{session('hapus')}}
                </div>
            </div>
        @endif
            {{-- <a class="btn btn-icon icon-left btn-info" href="{{url('guests/create')}}" name="btnIn" ><i class="fa fa-plus" aria-hidden="true" > Add Guest</i></a> --}}
            
            <div class="card-header table-responsive">
                <table  class="table table-hover table-striped table-bordered table-sm" style="width:200%;  solid black;">
                   
                    {{-- <table id="example1" class="table table-bordered table-striped"> --}}
                    @if(count($guests))
        
                    <thead class="thead-dark">
                        <tr>
                            <th width="30px" class="text-center" width="30" style="white-space: nowrap !important;background-color:#336699">No </th>
                            <th width="30px" class="text-center" scope="col" style="white-space: nowrap !important;background-color:#336699">ID Guest</th>
                            <th class="text-center" scope="col" style="white-space: nowrap !important;background-color:#336699" width: 60>Date In</th>
                            <th class="text-center" scope="col" style="white-space: nowrap !important;background-color:#336699">Date Out</th>
                            <th class="text-center" scope="col" style="white-space: nowrap !important;background-color:#336699">Nama </th>
                            <th class="text-center" scope="col" style="white-space: nowrap !important;background-color:#336699">Telephone </th>
                            <th class="text-center" scope="col" style="white-space: nowrap !important;background-color:#336699">Company </th>
                            <th class="text-center" scope="col" style="white-space: nowrap !important;background-color:#336699">Email</th>
                            <th class="text-center" scope="col" style="white-space: nowrap !important;background-color:#336699">Activity</th>
                            <th class="text-center" scope="col"  style="white-space: nowrap !important;background-color:#336699">No Rack </th>
                            <th class="text-center" scope="col" style="white-space: nowrap !important;background-color:#336699">No Loker</th>
                            <th  class="text-center" scope="col" style="white-space: nowrap !important;background-color:#336699">Foto </th>
                            <th class="text-center" scope="col" style="white-space: nowrap !important;background-color:#336699">Durasi </th>
                            <th class="text-center" scope="col" style="white-space: nowrap !important;background-color:#336699">Lokasi </th>
                            <th class="text-center" scope="col" style="white-space: nowrap !important;background-color:#336699">Ruangan </th>
                            <th class="text-center" scope="col" style="white-space: nowrap !important;background-color:#336699">Lantai </th>
                            <th class="text-center" scope="col" style="white-space: nowrap !important;background-color:#336699">Access </th>
                            <th class="text-center" scope="col" style="white-space: nowrap !important;background-color:#336699">Remarks </th>
                            
                            <th class="text-center" scope="col" style=" width: 10px; white-space: nowrap !important;background-color:#336699">Service of Maintaning Customer Data Security</th>
                            <th class="text-center" scope="col" style=" width: 10px; white-space: nowrap !important;background-color:#336699">Quality of Infrastructure</th>
                            <th class="text-center" scope="col" style=" width: 10px; white-space: nowrap !important;background-color:#336699">Service for In and Out Equipment</th>
                            <th class="text-center" scope="col" style=" width: 10px; white-space: nowrap !important;background-color:#336699">Service for Customer Visits to Data Center</th>

                            <th class="text-center" scope="col" style=" width: 10px; white-space: nowrap !important;background-color:#336699">Critics Or Suggestion</th>

                            <th width="30px" class="text-center" scope="col" style="white-space: nowrap !important;background-color:#336699" >Status </th>
                            {{-- <th  class="text-center" colspan="4" style="white-space: nowrap !important;background-color:#336699">Action</th> --}}
                        </tr>
                    </thead>
                        <tbody>
            @foreach ($guests as  $no => $guest)
                    <tr>
                      
                        
                        <td>{{ $guests->firstItem()+$no}} </td>
                        <td>{{$guest->guestsid}}</td>
                        <td>{{$guest->datein}}</td>
                        <td >{{$guest->dateout}}</td>
                        <td>{{$guest->name}}</td>
                        <td>{{$guest->telephone}}</td>
                        <td>{{$guest->company}}</td>
                        <td>{{$guest->email}}</td>
                        <td>{{$guest->activity}}</td>
                        <td>{{$guest->noRack}}</td>
                        <td>{{$guest->noLoker}}</td>
                        <td>
                            @if($guest->foto)
                            {{-- <a href="" onclick="window.open('/image/{{$guest->foto}}','targetWindow', 'toolbar=no, location=no, status=no, menubar=no, scrollbars=yes, resizable=yes, width=1090px, height=550px, top=25px left=120px' ); return false;">  --}}
                                {{-- <a data-fancybox="gallery" href="/image/{{$guest->foto}}"> --}}
                                    <a href="#" class="pop">
                                        <?php
                                            $foto = '/photos/photos/'.$guest->foto;
                                        ?>
                                    <img src="{{ url($foto) }}" width="60" height="60" alt="" ></a>
                                    @else
                                <i>NULL</i>
                            @endif
                        </td>
                        <td style="white-space: nowrap !important;">{{$guest->durasi}}</td>
                        {{-- <td>{{$guest->lokasi}}</td> --}}
                        <td>{{$guest->lokasi}}</td>
                        <td>{{$guest->ruang}}</td>
                        <td>{{$guest->lantai}}</td>
                        <td>{{$guest->access}}</td>
                        <td>{{$guest->remarks}}</td>
                        
                        <td>{{$guest->service_quality}}</td>
                        <td>{{$guest->infrastructure_quality}}</td>
                        <td>{{$guest->clean_quality}}</td>
                        <td>{{$guest->visitdatacenterint}}</td>
                        <td>{{$guest->critics_suggests}}</td>

                        <td> 
                            @if($guest->id_status==2)
                            <label class="btn btn-danger btn-md"  style="font-size:12px"> {{$guest->status}} </label>
                            @elseif($guest->id_status==1)
                            <label class="btn btn-success btn-md" style="font-size:12px"> {{$guest->status}} </label>
                            @else
                                <i>Null </i>
                            @endif

                        </td>
                        {{-- <td> 
                            <a href="{{ url("co/{$guest->id}/cek") }}" class="btn btn-outline-secondary"><i class="fa fa-calendar-times-o" aria-hidden="true"></i></a>
                        </td> --}}
                        {{-- <td > 
                            <a href="{{ url("guests/{$guest->id}/show") }}" class="btn btn-outline-info"><i class="fa fa-info-circle"></i></a>
                        </td> --}}
                        {{-- <td> 
                            <a href="{{ url("guests/{$guest->id}/edit") }}" class="btn btn-outline-success"><i class="fa fa-edit"></i></a>
                        </td>
                         --}}
                        {{-- <td>
                            <form action="{{ url("guests/{$guest->id}") }}" id="#" method="POST" onsubmit="return confirm('Yakin Hapus Data?')">
                                @csrf
                                @method('delete')
                                <button class="btn btn-outline-danger">
                                <i class="fa fa-trash"></i>
                                </button> 
                            </form>
                        </td> --}}
                       
                @endforeach
                   </tr>  
                </tbody>
                   @else
                        <h5> Data not found! </h5>
                        @endif
            </table>
            
            <div style="justify-content: center;">
                {{$guests->links()}}
            </div>
            
            {{-- {!! $guests->render()!!} --}}
            
            {{-- {!! $guests->render()!!} --}}
        Page : {{ $guests->currentPage() }} <br/>
        Total Data : {{ $guests->total() }} <br/>
        Data Per Page : {{ $guests->perPage() }}

    </div>

  
        <hr>
        
    
@endsection
    @push('page-scripts')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>

   @endpush

   @push('after-scripts')
   
  @endpush