@extends('guests.layoutnew')
@section('after_style')
@endsection
@section('content')
@include('guests.pesan')
<link href="{{asset('css/chooseimam.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/success.css')}}" rel="stylesheet" type="text/css">


<a class="navbar-brand text-white">
  <img src="{{ asset('images') }}/matrixlogo.png" alt="" width="120" height="50">
</a>
<br>
<br>
<br>
<html>
  <body>
    <div class="container">
      <div class="card" style="text-align: center; background: linear-gradient(to bottom left, #EF8D9C 40%, #FFC39E 100%); color: #fff; max-width: 250px; margin: auto;">
        <div class="card-body">
          <br>
          <br>
          <div class="dot"></div>
          <div class="dot two"></div>
          <div class="face2">
            <div class="eye"></div>
            <div class="eye right"></div>
            <div class="mouth sad"></div>
          </div>
          <br>
          <br>
          <br>
          <h5 class="card-title">Error!</h5>
          <h6>oh no, Data Not Found</h6>
        </div>
      </div>
    </div>
  </body>
</html>

  <script>
    setTimeout(function(){
      var url1 = '{{ route("getGuest") }}';
      window.location.href = url1;
        // '/guests/checkout';
    }, 3000);
</script>
      <!-- end of container -->
@endsection

@section('script')
@endsection