@extends('guests.layoutnew')
@section('after_style')
@endsection
@section('content')
@include('guests.pesan')
<link href="{{asset('css/backlight.css')}}" rel="stylesheet" type="text/css">
<a class="navbar-brand text-white">
    <img src="{{ asset('images') }}/matrixlogo.png" alt="" width="120" height="50">
  </a>
<br>
<br>
<br>
<div style="display: flex; justify-content: center;">
    <h3>List Guests</h3>
</div>
<br>

@if(session('message'))
<div class="alert alert-success" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    {{session('message')}}
</div>
@endif

<div class="container">
    <div class="card-deck">
        @if ($guests)
        @foreach ($guests as $guest)
        <div class="col-md-4" style="margin-bottom: 7%;">
            
            <div class="card">
                <img src="{{ url('/photos/photos/'.$guest->foto) }}" class="card-img-top" alt="...">
                <div class="card-body">
                    {{-- <a href="{{ route('foto', $guest->foto) }}" class="btn btn-sm btn-info shadow">
                        See Photo</i> 
                    </a> --}}
                    <h5 class="card-title text-dark">{{ $guest->name }}</h5>
                    <h5 class="card-title text-dark">{{ $guest->datein }}</h5>
                    <p class="card-text text-dark">Activity : {{ $guest->activity }}</p>
                    <p class="card-text text-dark">Number Rack: {{ $guest->noRack }}</p>
                    <p class="card-text text-dark">Number Locker: {{ $guest->noLoker }}</p>
                </div>
                <div class="card-footer" style="background-color: #151A48">
                    <small class="text-white" style="display: flex; justify-content: center;">{{ $guest->company }}</small>
                </div>
                <br>
            </div>
        </div>
        @endforeach

        <div style="display: flex; justify-content: center;">
            {!! $guests->links() !!}
        </div>
        
        @endif
      </div>
</div>

@endsection

@section('script')
@endsection

