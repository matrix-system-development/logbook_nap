@extends('guests.layoutnew')
@section('after_style')
@endsection
@section('content')
@include('guests.pesan')
{{-- <link href="{{asset('css/ratingkedua.css')}}" rel="stylesheet" type="text/css"> --}}
<link href="{{asset('css/ratingimam.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">



<div class="container" style="margin-top: 3%;">
    <form action="{{route('search')}}" method="POST" role="search">
        {{ csrf_field() }}
        <div class="input-group">
            <input type="text" class="form-control" name="q"
                placeholder="Find Guest"> <span class="input-group-btn">
                <button type="submit" class="btn btn-success"  style="margin-left: 10px;">
                    <span class="glyphicon glyphicon-search">Search!</span>
                </button>
            </span>
        </div>
    </form>
</div>
<form action="{{ route('checkout.process') }}" method="post" enctype="multipart/form-data">
@csrf
<div class="container">
    <div class="row">
        <div class="col-md-12">
            {{-- <div class="container" style="margin-top: 3%;">
                <h3 class="text-center" >Hasil Pencarian</h3>
            </div> --}}
            <header class='header text-center' style="margin-top: 10px;">
                <h3>How satisfied are you with our service?</h3>
                <p><b>Let us know, to improve our service performance</b></p>
            </header>
            @if ($guests)
            @foreach ($guests as $guest)
            <div class="container">
                <div class="card-deck">
                    <div class="col-md-4" style="margin-bottom: 7%;">
                        <div class="card">
                            <div class="card-body">
                                <div class="stars">
                                    <input class="star star-4" id="star-4" type="radio" name="service_quality" value="4"/>
                                    <label class="star star-4" for="star-4"></label>
                                    <input class="star star-3" id="star-3" type="radio" name="service_quality" value="3"/>
                                    <label class="star star-3" for="star-3"></label>
                                    <input class="star star-2" id="star-2" type="radio" name="service_quality" value="2"/>
                                    <label class="star star-2" for="star-2"></label>
                                    <input class="star star-1" id="star-1" type="radio" name="service_quality" value="1"/>
                                    <label class="star star-1" for="star-1"></label>
                                </div>
                            </div>
                            <div class="card-body">
                                <p style="text-align: center">
                                    Service of Maintaning Customer Data Security
                                </p>
                            </div>
                            <br>
                        </div>
                    </div>
                    <div class="col-md-4" style="margin-bottom: 7%;">
                        <div class="card">
                            <div class="card-body">
                                <div class="stars">
                                    <input class="star star-4" id="star-42" type="radio" name="infrastructure_quality" value="4"/>
                                    <label class="star star-4" for="star-42"></label>
                                    <input class="star star-3" id="star-32" type="radio" name="infrastructure_quality" value="3"/>
                                    <label class="star star-3" for="star-32"></label>
                                    <input class="star star-2" id="star-22" type="radio" name="infrastructure_quality" value="2"/>
                                    <label class="star star-2" for="star-22"></label>
                                    <input class="star star-1" id="star-12" type="radio" name="infrastructure_quality" value="1"/>
                                    <label class="star star-1" for="star-12"></label>
                                </div>
                            </div>
                            <div class="card-body">
                                <p style="text-align: center">
                                    Quality of Infrastructure
                                </p>
                                <br>
                            </div>
                            <br>
                        </div>
                    </div>
                    <div class="col-md-4" style="margin-bottom: 7%;">
                        <div class="card">
                            <div class="card-body">
                                <div class="stars">
                                    <input class="star star-4" id="star-43" type="radio" name="clean_quality" value="4"/>
                                    <label class="star star-4" for="star-43"></label>
                                    <input class="star star-3" id="star-33" type="radio" name="clean_quality" value="3"/>
                                    <label class="star star-3" for="star-33"></label>
                                    <input class="star star-2" id="star-23" type="radio" name="clean_quality" value="2"/>
                                    <label class="star star-2" for="star-23"></label>
                                    <input class="star star-1" id="star-13" type="radio" name="clean_quality" value="1"/>
                                    <label class="star star-1" for="star-13"></label>
                                </div>
                            </div>
                            <div class="card-body">
                                <p style="text-align: center">
                                    Service for In and Out Equipment
                                </p>
                                <br>
                            </div>
                            <br>
                        </div>
                    </div>
                    <div class="col-md-4" style="margin-bottom: 7%; margin-left: auto; margin-right:auto;">
                        <div class="card">
                            <div class="card-body">
                                <div class="stars">
                                    <input class="star star-4" id="star-444" type="radio" name="visitdatacenterint" value="4"/>
                                    <label class="star star-4" for="star-444"></label>
                                    <input class="star star-3" id="star-333" type="radio" name="visitdatacenterint" value="3"/>
                                    <label class="star star-3" for="star-333"></label>
                                    <input class="star star-2" id="star-222" type="radio" name="visitdatacenterint" value="2"/>
                                    <label class="star star-2" for="star-222"></label>
                                    <input class="star star-1" id="star-111" type="radio" name="visitdatacenterint" value="1"/>
                                    <label class="star star-1" for="star-111"></label>
                                </div>
                            </div>
                            <div class="card-body">
                                <p style="text-align: center">
                                    Service for Customer Visits to Data Center
                                </p>
                            </div>
                            <br>
                        </div>
                    </div>
                    </div>
            </div>

            <div hidden id="id">
                <input value="{{$guest->id}}" name="id" type="text" class="form-control bg-light"
                    id="id">
                </div>
            <div>
        </div>
        <div class="col-md-12">
            <div class="row mt-5 justify-content-center">
                <header class='header text-center' style="margin-top: 10px;">
                    <p><b>Give us critics and suggestions, to improve our service performance</b></p>
                </header>
                <div class="form-group col-md-12">
                    <textarea type="text" class="form-control" placeholder="Leave your comments" name="remarks" name="critics"></textarea>
                </div>
            </div>
        </div>

    </div>
    <div class="container"><br><br>
      <div class="card-deck">
        <div class="col-md-4" style="margin: auto ">
            <div class="card">
                <?php
                    $foto = '/photos/photos/'.$guest->foto;
                ?>
                <img class="card-img-top" src="{{ url($foto) }}" alt="...">
                {{-- <img src="{{ asset('/storage/photos/'.$guest->foto) }}" class="card-img-top" alt="..."> --}}
                <div class="card-body">
                    <h5 class="card-title text-dark">{{ $guest->name }}</h5>
                    <h5 class="card-title text-dark">{{ $guest->datein }}</h5>
                    <label>Date out : </label>
                    <div id="time" style="background-color: #ecf0f1; border: 1px dashed grey; height: auto; margin: 1px 0px; padding: 3px; text-align: left; width: auto;"></div>
                    <br>
                    <p class="card-text text-dark">Activity : {{ $guest->activity }}</p>
                    <p class="card-text text-dark">Number Rack: {{ $guest->noRack }}</p>
                    <p class="card-text text-dark">Number Locker: {{ $guest->noLoker }}</p>
                </div>
                <div class="card-footer" style="background-color: #151A48">
                    <div style="display: flex; justify-content: center;">
                        <button id="checkoutbutton" type="submit" class="btn btn-danger">Checkout</button>
                    </div>
                </div>
                <br>
            </div>
        </div>
      </div>
    </div>
    @endforeach
        {!! $guests->links() !!}
    @endif
</div>
</form>
<br>
<br>
<br>
    <!-- end of container -->
@endsection

@section('script')
<script type="text/javascript">
    $("#star-53").change(function () {
        $("#checkoutbutton").attr("disabled", true);
    });
    $("#star-43").change(function () {
        $("#checkoutbutton").attr("disabled", true);
    });
    $("#star-33").change(function () {
        $("#checkoutbutton").attr("disabled", true);
    });
    $("#star-23").change(function () {
        $("#checkoutbutton").attr("disabled", true);
    });
    $("#star-13").change(function () {
        $("#checkoutbutton").attr("disabled", true);
    });
    $("#star-52").change(function () {
        $("#checkoutbutton").attr("disabled", true);
    });
    $("#star-42").change(function () {
        $("#checkoutbutton").attr("disabled", true);
    });
    $("#star-32").change(function () {
        $("#checkoutbutton").attr("disabled", true);
    });
    $("#star-22").change(function () {
        $("#checkoutbutton").attr("disabled", true);
    });
    $("#star-12").change(function () {
        $("#checkoutbutton").attr("disabled", true);
    });
    $("#star-5").change(function () {
        $("#checkoutbutton").attr("disabled", true);
    });
    $("#star-4").change(function () {
        $("#checkoutbutton").attr("disabled", true);
    });
    $("#star-3").change(function () {
        $("#checkoutbutton").attr("disabled", true);
    });
    $("#star-2").change(function () {
        $("#checkoutbutton").attr("disabled", true);
    });
    $("#star-1").change(function () {
        $("#checkoutbutton").attr("disabled", true);
    });
    $("#star-444").change(function () {
        $("#checkoutbutton").attr("disabled", false);
    });
    $("#star-333").change(function () {
        $("#checkoutbutton").attr("disabled", false);
    });
    $("#star-222").change(function () {
        $("#checkoutbutton").attr("disabled", false);
    });
    $("#star-1111").change(function () {
        $("#checkoutbutton").attr("disabled", false);
    });
</script>
@endsection

