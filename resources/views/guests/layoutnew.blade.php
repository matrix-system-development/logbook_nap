<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <link href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.css" rel="stylesheet" type='text/css'>

    <!-- My CSS -->
    <link href="{{asset('css/background.css')}}" rel="stylesheet" type="text/css">


    <title>Guests Book</title>
    <link rel="shortcut icon" href="{{ asset('asset/img/Matrixlogo.png') }}" />
    @yield('after_style')

  </head>
  <body>

    <nav style="background-color: #151A48" class="navbar navbar-expand-lg navbar-dark fixed-top">
      
      <div id='stars'></div>
      <div id='stars2'></div>
      <div id='stars3'></div>
      
      {{-- <div class="container"> --}}
        <a class="navbar-brand text-white" style="margin-left: auto; margin-right: auto;">
          <img src="{{ asset('images') }}/matrixlogo.png" alt="" width="120" height="50">
        </a>
        
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link btn pull-center text-white" href="{{ route('guests') }}">Guests Book 
                <i class="fa fa-book"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link btn pull-center text-white" href="{{ route('chooseuser') }}">Add Guests 
                <i class="fa fa-group"></i>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link btn pull-center text-white" href="{{ route('checkout') }}">Checkout 
                <i class="fa fa-sign-out"></i>
              </a>
            </li>
          </ul>
        {{-- </div> --}}
      
      </div>
    </nav>


    


    <!-- footer -->
    @yield('content')
    <footer class="page-footer font-small bg-success" style="margin-top: 9%;"> <!-- 9-->
      <!-- Copyright -->
      <style>
        @media screen and (max-width: 620px) {
        .footer-copyright{
            margin-top: 135px;
        }
      }
      </style>
      <div class="footer-copyright text-center py-3 fixed-bottom" style="background-color: #151A48">
        <p class="text-white" style="margin-top: 5px;">&copy; PT NAP Info Lintas Nusa. All Rights Reserved. <?php echo date("Y"); ?></p><span class="text-white text-right">Version 1.0</span>
          <!-- <p class="text-white">Guests Book <a href="https://nap.net.id/home.html" class="text-white">PT NAP Info Lintas Nusa</a> </p>   -->
      </div>
      <script type="text/javascript">
          function showTime() {
              var date = new Date(),
                  utc = new Date(Date(
                      date.getFullYear(),
                      date.getMonth(),
                      date.getDate(),
                      date.getHours(),
                      date.getMinutes(),
                      date.getSeconds()
                  ));
              document.getElementById('time').innerHTML = utc.toLocaleString();
              //   document.getElementById('time').innerHTML = utc.toLocaleTimeString();
          }
          setInterval(showTime, 1000);
      </script>
      <script>
          function myfun() {
              var a = document.getElementById("mobilenumber").value;
              if (a.charCodeAt(0) == 57) {
                  document.getElementById("messages").innerHTML = "** Dimulai Dari 0 atau 62";
                  return false;
              }
          }
      </script>
      <script> 

          $("#test").on("input", function() {
            if (/^9/.test(this.value)) {
              this.value = this.value.replace(/^9/, "0")
            }
            if (/^8/.test(this.value)) {
              this.value = this.value.replace(/^8/, "0")
            }
            if (/^7/.test(this.value)) {
              this.value = this.value.replace(/^7/, "0")
            }
            if (/^61/.test(this.value)) {
              this.value = this.value.replace(/^61/, "62")
            }
            if (/^63/.test(this.value)) {
              this.value = this.value.replace(/^63/, "62")
            }
            if (/^64/.test(this.value)) {
              this.value = this.value.replace(/^64/, "62")
            }
            if (/^66/.test(this.value)) {
              this.value = this.value.replace(/^66/, "62")
            }
            if (/^67/.test(this.value)) {
              this.value = this.value.replace(/^67/, "62")
            }
            if (/^68/.test(this.value)) {
              this.value = this.value.replace(/^68/, "62")
            }
            if (/^69/.test(this.value)) {
              this.value = this.value.replace(/^69/, "62")
            }
            if (/^60/.test(this.value)) {
              this.value = this.value.replace(/^60/, "62")
            }
        
            if (/^5/.test(this.value)) {
              this.value = this.value.replace(/^5/, "0")
            }
            if (/^4/.test(this.value)) {
              this.value = this.value.replace(/^4/, "0")
            }
            if (/^3/.test(this.value)) {
              this.value = this.value.replace(/^3/, "0")
            }
            if (/^2/.test(this.value)) {
              this.value = this.value.replace(/^2/, "0")
            }
            if (/^1/.test(this.value)) {
              this.value = this.value.replace(/^1/, "0")
            }
            if (/^1/.test(this.value)) {
              this.value = this.value.replace(/^1/, "0")
            }
            if (/^+61/.test(this.value)) {
              this.value = this.value.replace(/^+61/, "+62")
            }
            if (/^+63/.test(this.value)) {
              this.value = this.value.replace(/^+63/, "+62")
            }
            if (/^+64/.test(this.value)) {
              this.value = this.value.replace(/^+64/, "+62")
            }
            if (/^+66/.test(this.value)) {
              this.value = this.value.replace(/^+66/, "+62")
            }
            if (/^+67/.test(this.value)) {
              this.value = this.value.replace(/^+67/, "+62")
            }
            if (/^+68/.test(this.value)) {
              this.value = this.value.replace(/^+68/, "+62")
            }
            if (/^+69/.test(this.value)) {
              this.value = this.value.replace(/^+69/, "+62")
            }
            if (/^+60/.test(this.value)) {
              this.value = this.value.replace(/^+60/, "+62")
            }
          })
          </script>           
      <!-- Copyright -->
  </footer>
  @yield('script')

   






    <script src="https://apis.google.com/js/platform.js"></script>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>
  </body>
</html>