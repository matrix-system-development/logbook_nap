@extends('guests.layoutnew')
@section('after_style')
@endsection
@section('content')
@include('guests.pesan')
<link href="{{asset('css/backlight.css')}}" rel="stylesheet" type="text/css">
<a class="navbar-brand text-white">
    <img src="{{ asset('images') }}/matrixlogo.png" alt="" width="120" height="50">
  </a>
<br>
<br>
<br>
<div style="display: flex; justify-content: center;">
    <h3>List Guests</h3>
</div>
<br>

@if(session('message'))
<div class="alert alert-success" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    {{session('message')}}
</div>
@endif

<div class="container">
    <img src="{{ url('/photos/photos/'.$foto) }}" class="card-img-top" alt="...">
</div>

@endsection

@section('script')
@endsection

