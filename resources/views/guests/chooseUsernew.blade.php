@extends('guests.layoutnew')
@section('after_style')
@endsection
@section('content')
@include('guests.pesan')
{{-- <link href="{{asset('css/rating2.css')}}" rel="stylesheet" type="text/css"> --}}

<link href="{{asset('css/chooseimam.css')}}" rel="stylesheet" type="text/css">
<script type="text/javascript" src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"
    integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous">
</script>

<a class="navbar-brand text-white">
  <img src="{{ asset('images') }}/matrixlogo.png" alt="" width="120" height="50">
</a>
<br>
<br>
<br>
<html>
  <body>
    <div class="container">
      <div class="card-deck">
        <div class="card" style="text-align: center; background: rgb(184, 180, 190); color: #151A48">
          <div class="card-body">
            <img src="{{ asset('images') }}/model.png" alt="" width="100" height="100" class="rounded-circle img-thumbnail">
            <br>
            <h5 class="card-title">New Customer?</h5>
            <h5 class="card-title">Register Here</h5>
          </div>
          <div class="card-footer">
            <button type="button" class="btn btn-dark"><a href="{{ route('guests.create') }}">Here!</a></button>
          </div>
        </div>
        <div class="card" style="text-align: center; background: #151A48; color: #fff;">
          <div class="card-body">
            <img src="{{ asset('images') }}/model.png" alt="" width="100" height="100" class="rounded-circle img-thumbnail">
            <br>
            <h5 class="card-title">Returning</h5>
            <h5 class="card-title">Customer</h5>
          </div>
          <div class="card-footer">
            <button type="button" class="btn btn-light"><a href="{{ route('getGuest')}}">Here!</a></button>
          </div>
        </div>
      </div>
    </div>
  </body>
  <br>
  <br>
  <br>
  <br>
  <br>
</html>

<!-- end of container -->
@endsection

@section('script')
@endsection
