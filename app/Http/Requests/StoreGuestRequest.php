<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreGuestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'guestsid' => 'required',
            'name' => 'required',
            'email' => 'required',
            'company' => 'required',
            'activity' => 'required',
            'foto' => 'required',
            // 'noRack' => 'required',
            // 'noLoker' => 'required',
            'telephone' => 'required|numeric'
        ];
    }

    //pesan error
    public function messages()
    {
        return [
            'guestsid.required' => 'Guestsid field is required.',
            'name.required' => 'name field is required.',
            'email.required' => 'email field is required.',
            'activity.required' => 'activity field is required.',
            // 'noRack.required' => 'noRack harus diisi',
            // 'noLoker.required' => 'noLoker harus diisi',
            'telephone.required' => 'telephone field is required.',
            'foto.required' => 'You have to take a picture of yours first.',
        ];
    }
}
